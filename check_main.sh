#!/bin/bash
#
# Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

out=out
build_file=$out/main.build
out_file=$out/main.out

mkdir -p out

make build-src -s > $build_file 2>&1

# Check empty output
if [ -s $build_file ]; then
	echo -ne "[\e[31mERR\e[0m] "
	echo -ne "\e[1mCOMPILING\e[0m "
	echo -e "(cat $build_file)"
	exit 1
fi

# Check main output
make run-main -s > $out_file 2>&1

if [ ! -f $out_file ]; then
	echo -ne "[\e[31mERR\e[0m] "
	echo -ne "\e[1mMAIN\e[0m "
	echo -e "(cat $out_file)"
	exit 1
fi

# Check diff
diff sav/main.res $out_file > out/main.diff
if [ -s out/main.diff ]; then
	echo -ne "[\e[31mKO\e[0m] "
	echo -ne "\e[1mMAIN\e[0m "
	echo -e "(diff sav/main.res $out_file)"
	exit 1
else
	echo -ne "[\e[32mOK\e[0m] "
	echo -e "\e[1mMAIN\e[0m"
	exit 0
fi
