/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * An upgrade that can be equipped by a Ship.
 */
public abstract class Upgrade implements Item {

    /**
     * Upgrade cost in credits.
     */
    Integer creditsCost;

    /**
     * How much energy is required to power this upgrade.
     */
    Integer energyCost;

    public Upgrade(Integer creditsCost, Integer energyCost) {
        this.creditsCost = creditsCost;
        this.energyCost = energyCost;
    }

    @Override
    public Integer getCreditsCost() {
        return creditsCost;
    }

    public Integer getEnergyCost() {
        return energyCost;
    }

    /**
     * Can this upgrade be used depending on the energy left in the ship and the
     * energy cost of the upgrade?
     *
     * @param usingShip the ship from which the upgrade is equipped.
     * @return true if the item can be used.
     */
    public Boolean canUse(Ship usingShip) {
        return usingShip.getEnergy() >= energyCost;
    }

    /**
     * Reload this upgrade.
     *
     * @param ship the ship to apply the upgrade on.
     *
     * By default, this method does nothing.
     */
    public void reload(Ship ship) {
    }

}
