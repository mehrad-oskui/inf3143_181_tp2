/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * A Blaster inflicts more hull damages than a Phaser but it needs to cool down.
 */
public class Blaster extends OffensiveUpgrade {

    /**
     * Hull damages to be inflicted by this weapon.
     */
    Integer damages;

    /**
     * Number of turns the weapon need to cool down between two uses.
     */
    Integer coolDownTime;

    /**
     * How many turns are left before this weapon can be fired again.
     *
     * Once this value reaches 0, the weapon can be fired.
     */
    Integer coolDown = 0;

    public Blaster(Integer damages, Integer coolDownTime) {
        super(damages * 100, damages / 10);
        this.damages = damages;
        this.coolDownTime = coolDownTime;
    }

    @Override
    public Boolean canUse(Ship firingShip) {
        return super.canUse(firingShip) && coolDown == 0;
    }

    @Override
    public Integer fire() {
        coolDown = coolDownTime;
        return damages;
    }

    @Override
    public void reload(Ship ship) {
        if (coolDown > 0) {
            coolDown--;
        }
    }

    public Integer getCoolDown() {
        return coolDown;
    }
    
    @Override
    public String toString() {
        return "blaster (dmg: " + damages + ", cd: " + coolDown + "/" + coolDownTime + ")";
    }

}
