/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * A Phaser deals a fixed amount of hull damages to a target.
 */
public class Phaser extends OffensiveUpgrade {

    /**
     * How much hull damages this weapon inflicts.
     */
    Integer damages;

    public Phaser(Integer damages) {
        super(damages * 10, damages / 10);
        this.damages = damages;
    }

    @Override
    public Integer fire() {
        return damages;
    }

    @Override
    public String toString() {
        return "phaser (dmg: " + damages + ")";
    }

}
